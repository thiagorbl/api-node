const fs = require('fs');
const courseController = require('../controllers/CoursesController');
const upload = require('../../common');

module.exports = (app) => {
    app.get('/', (req, res) => {
        fs.readFile('./index.html', (err, html) => res.end(html));
    });
app.post('/course', courseController.post);
app.post('/course/video', courseController.createVideoLesson);
app.put('/course/:id', courseController.put);
app.delete('/course/:id', courseController.delete);
app.get('/courses', courseController.get);
app.get('/course/:id', courseController.getById);
app.get('/courses/list/:courseId', courseController.getLessonList);
app.get('/courses/video/:courseId/:lessonNumber', courseController.getLesson);
app.post('/courses/upload/:id' , upload.single('video'), courseController.videoUpload);
app.get('/courses/:courseId/:lessonNumber', courseController.getVideo);
};
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const { json } = require('express/lib/response');
const db = require('./../../db');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');

// login 
exports.login = async (req, res, next) => {
    const{ email, password } = req.body;

    if(!email || !password) return res.status(400).send({msg: 'Campos inválidos'});

    const user = await db('users')
    .select('password')
    .where('email', email)
    .first();

    if (!user) return res.status(404).send({ error: 'User not Found!'    });

    if( !await bcrypt.compareSync(password, user.password) ){
        return res.status(401).send({ error: "Invalid Password" });   
    }

    const loggeduser = await db('users')
        .select('*')
        .where('email', email)
        .first();

    delete loggeduser.password

    const token = jwt.sign(
        { user: loggeduser.id },
        // nunca subir no git com as outras coisas
        "segredo", {
        expiresIn: 300
        });

    return res.status(200).send({ user: {...loggeduser}, token });
}



// create user
exports.post = (req, res, next) => {
    const body = req.body
    db("users").insert(body).then((data) => {
        res.status(201).send({
            ...body,
            id: data,
        });
    })
};

    


// update users
exports.put = async (req, res, next) => {
    let id = req.params.id;
    await db('users').update(req.body).where({ id: id});
    const updatdeuser = await db('users').where({ id: id});
    return res.status(200).json(...updatdeuser);
};


// delete user
exports.delete = (req, res, next) => {
    let id = req.params.id;
    db('users').del().where({ id: id}).then(() => {
        return res.status(200).json({ message: 'deleted' });
        })
    };


// get users list
exports.get = (req, res, next) => {
  db.select().table("users").then(data => {
    res.status(200).send(json.stringify(data))
})
};

exports.getById = (req, res, next) => {
    let id = req.params.id;
    db.select().table("users").where({
        "id": id
    }).then((data) => {
        if(data.length === 0){
            return res.status(400).json({ error: "user does not exist" });
        } else {
            res.status(200).send(data[0]);
        }
    });
};


const transport = nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "73b079b1e34f7a",
      pass: "8434bc22225daa"
    }
  });


  const usePasswordHashToMakeToken = ({
      password: passwordHash,
      id,
  }) => {
    const secret = `${passwordHash}-${id}`;
    const token = jwt.sign({id}, secret, {
        expiresIn: 3600, // em 1 hora ele expira
    });
    return token;

  };

  exports.sendPasswordResetEmail = async (req, res) => {
      const {email} = req.params;
      const user = await db('users')
        select('*')
        where('email', email)
        first();

    if (!user) {
        return res.status(404).json('não existe usuário para este email');
    }
    
    const token = usePasswordHashToMakeToken(user); //email de recuperação da senha
    const mailOptions = {
        to: email,
        subject: 'Recuperação de Senha',
        text: `Seu link de recuperação https://wwwurl.com/reset/${user.id}/${token}`,
    };

    const sendEmail = () => {
        transporter.sendMail(mailOptions, (err, data) => {
            if (err) {
                console.log('Error ${err}');
                return res.status(404).json('failed');
            }
            console.log('Email sent successfully', data.response);
            return res.status(200).json('sent');
        });
    };

    return sendEmail();

  };

  exports.receiveNewPassword = async (req, res) => {
    const { id, token } = req.params;
    const { password } = req.body;

    const user = await db('users')
        select('*')
        where('id', id)
        first();
    const secret = `${user.password}-${id}`;
    const payload = jwt.decode(token, secret);
    if (payload.id === user.id){
        const hash = await bcrypt.hashSync(password, 10);
        db('users').update({ password: hash }).where({ id }).then(() => res.status(202).json('Password changed accepted'));
        } else {
            res.status(404).json('Invalid user');
        }

    };

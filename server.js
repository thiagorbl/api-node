const express = require('express'); // ele fala que precisa do express 
const cors = require('cors'); // ele fala que precisa do cors 
const app = express(); // vai criar uma aplicação do express
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors());
app.use(express.json());
require('./src/Routes/index')(app);
app.listen(3333); // determinar o servidor que ele vai rodar

// ultima aula foi a 7